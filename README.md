## Deploy open5gs

### Deploy using openstack + terraform:
```shell
cd scripts
./create_openstack.sh --destroy
```

### Deploy using virtualbox + vagrant:
Before start, edit the script "vagrant/add_ubuntu_user.sh"  and add the correct public key

```shell
cd scripts
./create_vagrant.sh --destroy
```

### Create the subscribers
```shell
cd ../scripts
source configrc

# Connect to the http://<open5gs-cp>:3000/ User:  admin,  pass: 1423  and add the subscribers that are present in the file ../ansible/config.ym
# You can also create using the automated script (uses selenium), but you must add selenium driver and also install the browser driver:
./subscriber.py

```

### Start the test
```shell
cd ../scripts
source configrc
bash monitor.sh

# Connect to gnodeb:
gnodeb1
sudo su - 
start_genodeb.sh

# Connect to ue using session 1
ue
sudo su - 
start_ue01.sh

# Open another session and test the connectivity:
ue
sudo su - 
ip netns exec ue01 bash
routes.sh
ping 8.8.8.8

```
#!/usr/bin/python3

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
import time
import yaml
import mouse

mouse.move(0, 0)

with open("../ansible/vars.yml", "r") as stream:
	v = yaml.safe_load(stream)

with open("../ansible/config.yml", "r") as stream:
	c = yaml.safe_load(stream)

driver = webdriver.Firefox()
driver.get("http://" + str(v["open5gs_cp"]) + ":3000")


# Login	
WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("/html/body/div[1]/div/div[1]/div/div/div/div[2]/div[1]/input")).send_keys("admin")
WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("/html/body/div[1]/div/div[1]/div/div/div/div[2]/div[2]/input")).send_keys("1423" + Keys.RETURN)

# Delete all subscribers:

while True:		
	WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("/html/body/div[1]/div/div[1]/div/div/div/div[2]/div[1]/div[1]/div[2]")).click()
	try: 
		driver.find_element_by_css_selector(".delete > svg:nth-child(1) > g:nth-child(1) > path:nth-child(1)").click()
		WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("/html/body/div[1]/div/div[1]/div/div/div/div[2]/div[2]/div/div[7]/div[1]/span/div/div/div[2]/div[2]/button/div")).click()
	except: 		
		break

# Create subscribers:
for s in c["subscribers"]:
	try:
		time.sleep(1)
		WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("/html/body/div[1]/div/div[1]/div/div/div/div[2]/div[1]/div[1]/div[2]")).click()
		time.sleep(1)
		WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_css_selector(".gptr28-0 > svg:nth-child(1) > g:nth-child(1) > path:nth-child(1)")).click()
		time.sleep(1)
		WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("//*[@id=\"root_imsi\"]")).clear()
		time.sleep(1)
		WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("//*[@id=\"root_imsi\"]")).send_keys(str(s["imsi"]))
		time.sleep(1)
		WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("//*[@id=\"root_security_k\"]")).clear()
		time.sleep(1)
		WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("//*[@id=\"root_security_k\"]")).send_keys(str(s["key"]))
		time.sleep(1)
		WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("//*[@id=\"root_security_op_value\"]")).clear()
		time.sleep(1)
		WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("//*[@id=\"root_security_op_value\"]")).send_keys(str(s["op"]) + Keys.RETURN)
		time.sleep(1)
	except:
		try:
			time.sleep(1)
			WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("/html/body/div[1]/div/div[1]/div/div/div/div[2]/div[1]/div[1]/div[2]")).click()
			time.sleep(1)
			WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_css_selector(".gptr28-0 > svg:nth-child(1) > g:nth-child(1) > path:nth-child(1)")).click()
			time.sleep(1)
			WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("//*[@id=\"root_imsi\"]")).send_keys(str(s["imsi"]))
			time.sleep(1)
			WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("//*[@id=\"root_security_k\"]")).send_keys(str(s["key"]))
			time.sleep(1)
			WebDriverWait(driver,150).until(lambda driver: driver.find_element_by_xpath("//*[@id=\"root_security_op_value\"]")).send_keys(str(s["op"]) + Keys.RETURN)		
			time.sleep(1)
		except:
			pass



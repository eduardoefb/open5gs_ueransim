#!/usr/bin/python3

import json
inv = open("../ansible/hosts", "w")
v = open("../ansible/vars.yml", "w")
conf = open("configrc", "w")

o = json.load(open("../terraform/terraform.tfstate", "r"))



for i in o["outputs"]["vm_ips"]["value"]:
    inv.write("[" + str(i) + "]\n")
    inv.write(o["outputs"]["vm_ips"]["value"][i] + "\n\n") 
    v.write(str(i) + ": " + str(o["outputs"]["vm_ips"]["value"][i]) + "\n")
    conf.write("alias " + str(i) + "=\"ssh -o StrictHostKeyChecking=no ubuntu@" + str(o["outputs"]["vm_ips"]["value"][i]) + "\"\n")

inv.write("[nodes]\n")
v.write("nodes:\n")
for i in o["outputs"]["vm_ips"]["value"]:    
    inv.write(o["outputs"]["vm_ips"]["value"][i] + "\n") 
    v.write("  - " + str(o["outputs"]["vm_ips"]["value"][i]) + "\n")


for i in o["resources"]:
    if i["type"] == "openstack_compute_floatingip_associate_v2":
        for j in i["instances"]:
            v.write(j["index_key"]+"_int: " + j["attributes"]["fixed_ip"] + "\n")
            

inv.close()
v.close()
conf.close()
#!/bin/bash

CWD=`pwd`
# Create infra:
cd ../vagrant

if [ "$1" == "--destroy" ]; then
   vagrant destroy --force
fi
vagrant up

# Update inventory:
cat hosts > ../ansible/hosts
cat vars.yml > ../ansible/vars.yml
cat configrc > ../ansible/configurc
cd ${CWD}

cd ../ansible
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts build.yml
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts configure.yml

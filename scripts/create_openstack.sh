#!/bin/bash

CWD=`pwd`
# Create infra:
cd ../terraform
terraform init

if [ "$1" == "--destroy" ]; then
   terraform destroy --auto-approve
fi
terraform apply --auto-approve
terraform apply --auto-approve
terraform apply --auto-approve

# Update inventory:
cd ${CWD}
python3 inventory.py

cd ../ansible
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts build.yml
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts configure.yml

#!/bin/bash

# Create infra:
terraform apply --auto-approve

# Update inventory:
python3 inventory.py

# Install dep
# ansible-galaxy collection install community.general

# Start deployment:
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts build.yml

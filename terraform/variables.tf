
variable sec_group_rules{
    type = map(object({direction = string, ethertype = string, protocol = string, port_range_min = string, port_range_max = string, remote_ip_prefix = string }))
    default = {
        rule1 = {
            direction = "ingress"
            ethertype = "IPv4"
            protocol = "tcp"
            port_range_min = "22"
            port_range_max = "9000"
            remote_ip_prefix = "0.0.0.0/0"
        }
        rule2 = {
            direction = "ingress"
            ethertype = "IPv4"
            protocol = "sctp"
            port_range_min = "38412"
            port_range_max = "38412"
            remote_ip_prefix = "0.0.0.0/0"
        }
        rule3 = {
            direction = "ingress"
            ethertype = "IPv4"
            protocol = "udp"
            port_range_min = "22"
            port_range_max = "9000"
            remote_ip_prefix = "0.0.0.0/0"
        }
        rule4 = {
            direction = "ingress"
            ethertype = "IPv4"
            protocol = "icmp"
            port_range_min = "0"
            port_range_max = "0"
            remote_ip_prefix = "0.0.0.0/0"
        }   
        rule5 = {
            direction = "ingress"
            ethertype = "IPv4"
            protocol = "tcp"
            port_range_min = "5060"
            port_range_max = "5060"
            remote_ip_prefix = "0.0.0.0/0"
        }                             
    }
}

variable public_key {
    type = object({name = string, file = string})
    default = {
        name = "oai"
        file = "/home/eduardoefb/.ssh/id_rsa.pub"
    }
}

variable domain {
    type = map(string)
    default = {
        zone = "open5gs.int."
    }
}


variable oam_network {
    type = object({external_network = string, name = string, subnet_name = string, subnet = string, gateway = string, router = string, dns_servers = list(string)})

    default = { 
        external_network = "kubespray"
        name = "open5gs"
        subnet_name = "open5gs_sub_v4"
        subnet = "172.31.0.0/28"
        dns_servers = [ "10.2.1.30" ]
        gateway = "172.31.0.1"
        router = "5g_router"
    }
}

variable vms {
    type = map(object({name = string, flavor = string, image = string, volume_size = string, networks = list(string)}))
    default = { 
            open5gs_cp  = { name = "open5gs_cp",  flavor = "m1.xlarge", image = "ubuntu_18.04", volume_size = "160", networks = [ "open5gs"] },
            open5gs_up1  = { name = "open5gs_up1",  flavor = "m1.xlarge", image = "ubuntu_18.04", volume_size = "160", networks = [ "open5gs"] },
            gnodeb1  = { name = "gnodeb1",  flavor = "m1.large", image = "ubuntu_18.04", volume_size = "160", networks = [ "open5gs"] },
            ue  = { name = "ue",  flavor = "m1.large", image = "ubuntu_18.04", volume_size = "160", networks = [ "open5gs"] },            
            kamailio  = { name = "kamailio",  flavor = "m1.xlarge", image = "ubuntu_18.04", volume_size = "160", networks = [ "open5gs"] },           
    }
}

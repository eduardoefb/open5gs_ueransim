
resource "openstack_compute_instance_v2" "vms" {
  for_each = var.vms

  name            = each.value.name
  flavor_name       = each.value.flavor
  # security_groups = [ openstack_compute_secgroup_v2.security_group01.name ]
  security_groups = [ openstack_networking_secgroup_v2.secgroup_1.name ]  
  key_pair = openstack_compute_keypair_v2.keypair.name

  block_device {
    uuid                  = data.openstack_images_image_v2.img[each.key].id
    source_type           = "image"
    volume_size           = each.value.volume_size
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  dynamic network {
    for_each = each.value.networks
    content{
        name   = network.value
    }
  }

  depends_on = [
    openstack_networking_subnet_v2.oam_sub
  ]
}

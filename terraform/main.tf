# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

resource "openstack_compute_keypair_v2" "keypair" {
  name       = var.public_key.name
  public_key = file(var.public_key.file)  
}


data "openstack_images_image_v2" "img" {
  for_each = var.vms 
  name = each.value.image
}

output "vm_hostnames"{  
  value = tomap({
    for k, bd in openstack_dns_recordset_v2.recordsets : k => bd.name
  })  
}

output "vm_ips"{
  value = tomap({
    for k, bd in openstack_networking_floatingip_v2.floating_ip : k => bd.address
  })
}




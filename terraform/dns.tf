
resource "openstack_dns_zone_v2" "testvm" {
  name        = var.domain.zone
  email       = "jdoe@example.com"
  description = "An example zone"
  ttl         = 3000
  type        = "PRIMARY"
}

resource "openstack_dns_recordset_v2" "recordsets" {
  for_each = var.vms
  zone_id     = openstack_dns_zone_v2.testvm.id
  name        = "${each.key}.${openstack_dns_zone_v2.testvm.name}"
  description = "An example record set"
  ttl         = 3000
  type        = "A"
  records     = ["${openstack_networking_floatingip_v2.floating_ip[each.key].address}"]
}

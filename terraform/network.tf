

resource "openstack_networking_secgroup_v2" "secgroup_1" {
  name        = "secgroup_1"
  description = "My neutron security group"
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_1" {
    for_each = var.sec_group_rules
      direction         = each.value.direction
      ethertype         = each.value.ethertype
      protocol          = each.value.protocol
      port_range_min    = each.value.port_range_min
      port_range_max    = each.value.port_range_max
      remote_ip_prefix  = each.value.remote_ip_prefix
      security_group_id = "${openstack_networking_secgroup_v2.secgroup_1.id}"
}

resource "openstack_networking_network_v2" "oam" {
  name           = var.oam_network.name
  admin_state_up = "true"
}


resource "openstack_networking_subnet_v2" "oam_sub" {
  name              = var.oam_network.subnet_name
  network_id        = openstack_networking_network_v2.oam.id
  cidr              = var.oam_network.subnet
  dns_nameservers   = var.oam_network.dns_servers
  ip_version        = 4
  gateway_ip        = var.oam_network.gateway
  
}


data "openstack_networking_network_v2" "external_network" {
  name = var.oam_network.external_network
}

data "openstack_networking_subnet_v2" "ext_subnets" {
  network_id = data.openstack_networking_network_v2.external_network.id
}


resource "openstack_networking_router_v2" "oam_router" {
  name                = var.oam_network.router
  admin_state_up      = true
  external_network_id = data.openstack_networking_subnet_v2.ext_subnets.network_id
}

resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = openstack_networking_router_v2.oam_router.id
  subnet_id = openstack_networking_subnet_v2.oam_sub.id
}

resource "openstack_networking_floatingip_v2" "floating_ip" {
  for_each = var.vms
  pool       = data.openstack_networking_network_v2.external_network.name
  subnet_id =  data.openstack_networking_subnet_v2.ext_subnets.id
}

resource "openstack_compute_floatingip_associate_v2" "myip" {
  for_each = var.vms

  floating_ip = openstack_networking_floatingip_v2.floating_ip[each.key].address
  instance_id = openstack_compute_instance_v2.vms[each.key].id
  fixed_ip    = openstack_compute_instance_v2.vms[each.key].network.0.fixed_ip_v4
}
